
# -- Paquetes que vamos a importar
import webApp
from urllib.parse import parse_qs
import secrets


# -- Diferentes códigos html para la pagina

PAGE_GET = """
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
    </head>
    <body>
        <div style="align-items:center; text-align:center;">
            <h1>¡Bienvenido a RandomShort!</h1>
            <form action="/" method="POST" style="text-align:center; display:flex; 
            flex-direction:column; align-items:center;"
                <label for="url">Escribe una URL:</label>
                <input name="url" id="url" type="text" required style="margin-bottom:10px;">
                <input type="submit" name="boton" value="Acortar!">
                
            </form>
            <p>Todas las paginas acortadas son: {paginas}</p>
        </div>
    </body>
</html>
"""

PAGE_POST = """
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
    </head>
    <body>
        <div style="align-items:center; text-align:center;">
            <h1>Las paginas acortadas son:</h1>
            <p>{paginas}</p>
            <p> Para poder dirigirte a la pagina acortada pon en la url el recurso: {nuevo_recurso}</p>
        </div>
    </body>
"""

PAGE_404 = """
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
    </head>
    <body>
        <h1 style="color:#FF6347; text-align:center;"> 404 Recurso no existe! </h1>
    </body>
"""

PAGE_NOT_ALLOWED = """
<!DOCTYPE html>
<html lang="es">
  <body>
    <p>Method not allowed: {method}.</p>
  </body>
</html>
"""

PAGE_UNPROCESABLE = """
<!DOCTYPE html>
<html lang="es">
  <body>
    <p>Unprocesable POST: {body}.</p>
  </body>
</html>
"""

diccionario_urls = {}

class RandomShort(webApp.webApp):

    def parse(self, peticion):
        # -- Coge de la petición la informacion que queremos,
        # -- Queremos el recurso, el metodo y el cuerpo
        metodo = peticion.split(' ')[0]
        recurso = peticion.split(' ')[1][1:]
        if metodo == "POST":
            cuerpo = peticion.split("\r\n\r\n")[-1]
        else:
            cuerpo = None
        return metodo, recurso, cuerpo

    def process(self, parsedrequest):
        metodo, recurso, cuerpo = parsedrequest

        if metodo == "GET":
            cuerpo_html, codigo_http = self.do_GET(recurso)
        elif metodo == "POST":
            cuerpo_html, codigo_http = self.do_POST(cuerpo)
        else:
            cuerpo_html = PAGE_NOT_ALLOWED.format(metodo=metodo)
            codigo_http = '405 Method not allowed'

        return codigo_http, '<html><body>' + cuerpo_html + '</body></html>'

    @staticmethod
    def do_GET(recurso):
        if recurso == "" or recurso == '/':
            paginas = ", ".join(diccionario_urls.values())
            cuerpo_html = PAGE_GET.format(paginas=paginas)
            codigo_http = '200 OK'
        elif recurso in diccionario_urls.keys():
            nueva_url = diccionario_urls[recurso]
            cuerpo_html = " "
            codigo_http = '302 Found\r\n' + f'Location: {nueva_url}'
        else:
            cuerpo_html = PAGE_404
            codigo_http = '404 Not Found'
        return cuerpo_html, codigo_http

    @staticmethod
    def transformar_pagina(diccionario_cuerpo, nuevo_recurso, paginas):
        http = diccionario_cuerpo["url"][0].split('//')
        if http[0] == "http:" or http[0] == "https:":
            diccionario_urls[nuevo_recurso] = diccionario_cuerpo["url"][0]
            cuerpo_html = PAGE_POST.format(paginas=paginas, nuevo_recurso=nuevo_recurso)
            codigo_http = "200 OK"
        else:
            pagina = "https://" + diccionario_cuerpo["url"][0]
            diccionario_urls[nuevo_recurso] = pagina
            cuerpo_html = PAGE_POST.format(paginas=paginas, nuevo_recurso=nuevo_recurso)
            codigo_http = "200 OK"
        return cuerpo_html, codigo_http

    @staticmethod
    def buscar_clave(diccionario_cuerpo):
        for clave, valor in diccionario_urls.items():
            if valor in diccionario_cuerpo["url"][0]:
                return clave
            else:
                return None

    def do_POST(self, cuerpo):
        diccionario_cuerpo = parse_qs(cuerpo)
        paginas = ", ".join(diccionario_urls.values())
        if ("url" in diccionario_cuerpo) and ("boton" in diccionario_cuerpo):
            if ",".join(diccionario_cuerpo["url"]) in paginas:
                recurso = self.buscar_clave(diccionario_cuerpo)
                cuerpo_html = PAGE_POST.format(paginas=paginas, nuevo_recurso=recurso)
                codigo_http = "200 OK"
            else:
                nuevo_recurso = secrets.token_urlsafe(16)
                cuerpo_html, codigo_http = self.transformar_pagina(diccionario_cuerpo, nuevo_recurso, paginas)
        else:
            cuerpo_html = PAGE_UNPROCESABLE.format(cuerpo=cuerpo)
            codigo_http = '422 Unprocessable Entity'
        return cuerpo_html, codigo_http


if __name__ == '__main__':
    testWebApp = RandomShort("localhost", 1234)
